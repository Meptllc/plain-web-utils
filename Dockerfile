FROM jekyll/jekyll:3.8 as builder

COPY app .
RUN mkdir /build && chown jekyll:jekyll /build \
    && jekyll build --destination /build

FROM nginx:1.15

# Copy files into the image
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /build /app
WORKDIR /app

EXPOSE 80
# Inherit CMD from nginx
